package com.mattgu.wifip2ptx;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

/**
 * A BroadcastReceiver that notifies of important Wi-Fi p2p events.
 */
class WiFiDirectBroadcastReceiver extends BroadcastReceiver implements WifiP2pManager.PeerListListener, WifiP2pManager.GroupInfoListener {

    private static final String LOG_TAG = WiFiDirectBroadcastReceiver.class.getSimpleName();
    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private MainActivity mActivity;
    public WifiP2pDeviceList peers;

    public WiFiDirectBroadcastReceiver(WifiP2pManager manager, WifiP2pManager.Channel channel,
                                       MainActivity activity) {
        super();
        this.mManager = manager;
        this.mChannel = channel;
        this.mActivity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        mActivity.updateIpAdress();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            // Check to see if Wi-Fi is enabled and notify appropriate activity
            Log.d(LOG_TAG, "Receive WIFI_P2P_STATE_CHANGED_ACTION");
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                // Wifi P2P is enabled
                Log.d(LOG_TAG, "Wifi P2P is enabled");
            } else {
                // Wi-Fi P2P is not enabled
                Log.d(LOG_TAG, "Wifi P2P is not enabled");
            }
        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
            // Call WifiP2pManager.requestPeers() to get a list of current peers
            Log.d(LOG_TAG, "Receive WIFI_P2P_PEERS_CHANGED_ACTION");
            // request available peers from the wifi p2p manager. This is an
            // asynchronous call and the calling activity is notified with a
            // callback on PeerListListener.onPeersAvailable()
            if (mManager != null) {
                mManager.requestPeers(mChannel, this);
            }
        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            // Respond to new connection or disconnections
            Log.d(LOG_TAG, "Receive WIFI_P2P_CONNECTION_CHANGED_ACTION");
            // request groupInfo
            if (mManager != null) {
                mManager.requestGroupInfo(mChannel, this);
            }
        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            // Respond to this device's wifi state changing
            Log.d(LOG_TAG, "Receive WIFI_P2P_THIS_DEVICE_CHANGED_ACTION");
        }
    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList peers) {
        Log.d(LOG_TAG, "Peers: " + peers.toString());
        this.peers = peers;
        mActivity.updatePeers();
    }

    @Override
    public void onGroupInfoAvailable(WifiP2pGroup group) {
        Log.d(LOG_TAG, "WifiP2PGroup: " + group);
        if(group != null && group.isGroupOwner()) {
            Log.d(LOG_TAG, "I am group owner");
        }
    }
}