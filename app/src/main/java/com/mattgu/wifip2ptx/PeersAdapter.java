package com.mattgu.wifip2ptx;

import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PeersAdapter extends ArrayAdapter<WifiP2pDevice> {
    MainActivity context;
    public PeersAdapter(MainActivity context, ArrayList<WifiP2pDevice> peers) {
        super(context, 0, peers);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final WifiP2pDevice peer = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.peer_item, parent, false);
        }
        // Lookup view for data population
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView adress = (TextView) convertView.findViewById(R.id.adress);
        convertView.findViewById(R.id.peer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Start connecting to " + peer.deviceName, Toast.LENGTH_SHORT).show();
                //obtain a peer from the WifiP2pDeviceList
                WifiP2pConfig config = new WifiP2pConfig();
                config.deviceAddress = peer.deviceAddress;
                context.mManager.connect(context.mChannel, config, new WifiP2pManager.ActionListener() {

                    @Override
                    public void onSuccess() {
                        Toast.makeText(context, "Connection success with " + peer.deviceName, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int reason) {
                        Toast.makeText(context, "Connection failure with " + peer.deviceName + " (" + reason + ")", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        // Populate the data into the template view using the data object
        name.setText(peer.deviceName);
        adress.setText(peer.deviceAddress);
        // Return the completed view to render on screen
        return convertView;
    }
}